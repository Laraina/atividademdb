package edu.ifpb.dac;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSRuntimeException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;

/**
 *
 * @author Ricardo Job
 */
//@MessageDriven(mappedName = "java:global/jms/demoQueue")
@MessageDriven(
        mappedName = "java:global/jms/demoQueue",
        activationConfig = {
            @ActivationConfigProperty(propertyName = "destinationType",
                    propertyValue = "javax.jms.Queue"),})

public class ServidorCartao implements MessageListener {

    //     @EJB
//     private RespostaCartao rc;   
    @Inject
    private JMSContext context;

    @Resource(lookup = "java:global/jms/outQueue")
    Queue canalDeResposta;

    public void notifica(String resp) throws JMSException {
//         Destination dest = canalDeResposta;
//         obj.setJMSReplyTo(dest);
        Message mensagem = context.createTextMessage(resp);
        context.createProducer().send(canalDeResposta, mensagem);
    }

    @Override
    public void onMessage(Message message) {
        try {
            String resp = "Aprovado!";
            ObjectMessage objectMessage = (ObjectMessage) message;
            Mensagem pedido = (Mensagem) objectMessage.getObject();
             System.out.println("Verificando situação do Cartão: "+ pedido.getCartao());
             //só há 50 reais de saldo no cartão!
            if (pedido.getValor() > 50) {
                resp = " Não aprovado!";
            }
            notifica(resp);
        } catch (JMSException ex) {
            Logger.getLogger(ServidorCartao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
