/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Laraina
 */
@Stateless
public class Cadastro {
    
    @PersistenceContext(unitName="AtividadeSBPU")
    private EntityManager em;

    public Cadastro() {
    }
    
    public void salvar(Produto p){
            em.persist(p);
    }
    
     public List<Produto> listar(){
        List<Produto> lista = em.createQuery("SELECT p FROM Produto p").getResultList();
        return lista;           
        }
   
}
