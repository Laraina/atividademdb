package edu.ifpb.dac;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSRuntimeException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateful
@Remote(Carrinho.class)
public class CarrinhoDeCompra implements Carrinho {

    @PersistenceContext(unitName = "AtividadeSBPU")
    private EntityManager em;

    @Inject
    private JMSContext context;

    @Resource(lookup = "java:global/jms/demoQueue")
    Queue canalDeDestino;

    private List<Produto> pedido = new ArrayList<>();

    public List<Produto> getPedido() {
        return pedido;
    }

    @Override
    public void add(Produto produto) {
        pedido.add(produto);
    }

    @Override
    public void del(Produto prod) {
        //inexplicavelmente o remove não funciona :(
        // pedido.remove(prod);
        for (Produto pedido1 : pedido) {
            if ((prod.getDescricao().equals(pedido1.getDescricao())) && (prod.getPreco().equals(pedido1.getPreco()))) {
                this.pedido.remove(pedido1);
            }
        }

    }

    @Override
    public List<Produto> listaDeProdutos() {
        return pedido;
    }

    @Override
    public void cancelar() {
        pedido.removeAll(pedido);
    }

    @Override
    public int finalizar(Pedido p) {
        int total = 0;
        total = pedido.size();
        pedido.removeAll(pedido);
        return total;
    }

    public void enviarMensagem(Pedido ped) throws JMSException {
        try {
            float total = 0;
            for (Produto produto : pedido) {
                total += produto.getPreco().floatValue();
            }
            Mensagem msg = new Mensagem();
            msg.setCartao("121212");
            msg.setValor(total);
            ObjectMessage objeto = context.createObjectMessage(msg);
            context.createProducer().send(canalDeDestino, objeto);
            System.out.println("Enviado para verificação!");

        } catch (JMSRuntimeException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String retornaResposta() throws JMSException {
        return RespostaCartao.resposta;
    }

    @Override
    public void salvarPedido(Pedido p) {
        try {
            em.persist(p);
            enviarMensagem(p);
        } catch (JMSException ex) {
            Logger.getLogger(CarrinhoDeCompra.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
