
package edu.ifpb.dac;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.jms.JMSConnectionFactoryDefinition;
import javax.jms.JMSConnectionFactoryDefinitions;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.JMSException;

/**
 *
 * @author Laraina
 */


@JMSDestinationDefinitions({
    @JMSDestinationDefinition(
        name = "java:global/jms/demoQueue",
        description = "Queue  de envio",
        interfaceName = "javax.jms.Queue"
        ),
    @JMSDestinationDefinition(
        name = "java:global/jms/outQueue",
        description = "Queue de retorno",
        interfaceName = "javax.jms.Queue")
})

@JMSConnectionFactoryDefinitions({
   @JMSConnectionFactoryDefinition(
        name = "java:global/jms/demoConnectionFactory",
        description = "ConnectionFactory de envio"
),
    @JMSConnectionFactoryDefinition(
        name = "java:global/jms/outConnectionFactory",
        description = "ConnectionFactory de retorno"
)
})


@Named(value = "controlador")
@javax.enterprise.context.SessionScoped
public class Controlador implements Serializable {

    @EJB
    private Carrinho carrinho;
    @EJB
    private Cadastro cadastro;
    private Produto produto;
    private List<Produto> produtos;
    private Pedido pedido;
    private Produto destaque;
    private String msg; 

    public Controlador() {
        produto = new Produto();
        pedido = new Pedido();
        produtos= new ArrayList<>();
    }

    public Carrinho getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(Carrinho carrinho) {
        this.carrinho = carrinho;
    }

    public Produto getDestaque() {
        return destaque;
    }
    
    public Cadastro getCadastro() {
        return cadastro;
    }

    public void setCadastro(Cadastro cadastro) {
        this.cadastro = cadastro;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public List<Produto> getProdutos() {
        return cadastro.listar();
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
    
    //Metodos     
     public String addProduto() {
         cadastro.salvar(produto);
         produtos.add(produto);
         produto = new Produto();        
         return "index";
    }
     
     public void produtoDestaque() {         
         Random gerador = new Random();
         int indice = gerador.nextInt(getProdutos().size());
         destaque = getProdutos().get(indice);
         destaque.setDescricao(destaque.getDescricao()+", R$: ");
    }

    public String addCarrinho(Produto p) {
          carrinho.add(p);
          pedido.setProdutos(carrinho.listaDeProdutos());
          produto = new Produto();
          produtoDestaque();
          return null;
    }
    
       
    public String remover(Produto prod) {
        carrinho.del(prod);        
        pedido.setProdutos(carrinho.listaDeProdutos());
        return null;
    }

    public String finalizar() {
        carrinho.salvarPedido(pedido);
        int total = carrinho.finalizar(pedido);
        FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage("Pedido com "+ total +" produto(s) registrado com sucesso!!!"));
        return null;
    }
    
      public String retornar() throws JMSException {
        msg = carrinho.retornaResposta();
        FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage( msg +"!!"));
        return null;
    }
    
    public String cancelar() {
        carrinho.cancelar();
        FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage("Pedido cancelado com sucesso!!!"));
        return null;
    }
    


}

